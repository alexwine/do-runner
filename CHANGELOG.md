## [1.4.1](https://gitlab.com/alexwine/do-runner/compare/v1.4.0...v1.4.1) (2022-05-10)


### Bug Fixes

* **cli:** potential fix for do-cli functioning ([e888cc9](https://gitlab.com/alexwine/do-runner/commit/e888cc9909b718eb84bd02bac6e547a08cd948b9))

# [1.4.0](https://gitlab.com/alexwine/do-runner/compare/v1.3.0...v1.4.0) (2022-05-10)


### Bug Fixes

* **digital ocean checker:** removed unnecessary log ([c1ab118](https://gitlab.com/alexwine/do-runner/commit/c1ab1181b932276190a1792b15408028599eefea))


### Features

* **cli:** updated version and added base ci job ([9da13b3](https://gitlab.com/alexwine/do-runner/commit/9da13b34972199025e69c5fb87cd2408de61e108))

# [1.3.0](https://gitlab.com/alexwine/do-runner/compare/v1.2.0...v1.3.0) (2022-05-10)


### Features

* **do-runner:** added mysql:check function ([14a2ab2](https://gitlab.com/alexwine/do-runner/commit/14a2ab2a2ce6f39d2f1fb46f1adf6e56006371d4))

# [1.2.0](https://gitlab.com/alexwine/do-runner/compare/v1.1.0...v1.2.0) (2022-05-10)


### Features

* **package:** testing package implementation ([9ffe076](https://gitlab.com/alexwine/do-runner/commit/9ffe0761c2ed25b376dc9d73e5c014253e254fd6))
* **runner:** fixed implementation of runner package ([69703b6](https://gitlab.com/alexwine/do-runner/commit/69703b6f1d0ff1c7e80a0ef99b7b26af5acafdfd))
* **runner:** scaffolded base runner project ([ff0baad](https://gitlab.com/alexwine/do-runner/commit/ff0baad0e9d5e39ed48f8df04bb5d9d4fd5bf616))
* **to be deleted:** to be deleted ([71f8323](https://gitlab.com/alexwine/do-runner/commit/71f8323017665132b649ca23c4d1471e6bc68e36))
* **utilities:** added general utility package ([45c6a83](https://gitlab.com/alexwine/do-runner/commit/45c6a8358073339a3a8e329b42d71ac066c27e39))

# [1.1.0](https://gitlab.com/alexwine/do-runner/compare/v1.0.0...v1.1.0) (2022-05-09)


### Features

* **packages:** updated to use recommeded data-access format ([7667ddc](https://gitlab.com/alexwine/do-runner/commit/7667ddc94160c42248b5cbbd1fd406a98af4d6f9))

# 1.0.0 (2022-05-09)


### Features

* **initial:** initial Commit ([6222d96](https://gitlab.com/alexwine/do-runner/commit/6222d96ad0f9159a72048e13ad03342708bb6b11))
* **mysql:** added small MySQL ping lib ([bcece81](https://gitlab.com/alexwine/do-runner/commit/bcece8156414c8b0cc534491a6e904d1c850f0dc))
