import { DigitalOceanChecker } from '@do-runner/data-access-digital-ocean';
import { runner } from '@do-runner/runner';
// "../../../../data-access/digital-ocean/src";

export const dropletsGet = async (token?: string) => {
  const drop = DigitalOceanChecker.getInstance();
  // console.log(token, drop);
  const res = await drop.getDroplets();
  console.log(res);
};

export const runCheck = async (droplet_name: string) => {
  const res = await runner({
    droplet_name,
  });
  console.log(res);
};
