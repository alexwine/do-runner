#!/usr/bin/env node

// import chalk = require('chalk');
import * as chalk from 'chalk';
import clear from 'clear';
// import path from 'path';
import { Command } from 'commander';
import * as figlet from 'figlet';
import def from '../../package.json';
import { dropletsGet, runCheck } from './commands';
// import clear = require('clear');

clear();
console.log(
  chalk.blue(figlet.textSync('DO Rebooter', { horizontalLayout: 'full' }))
);

export async function main() {
  const program = new Command();
  program
    .version(def.version)
    .description("An example CLI for ordering pizza's");
  program
    .command('droplets:get')
    .option('-t', '--token', 'Digital Ocean API Token')
    .action(dropletsGet);

  program.command('mysql:check <dropletname>').action(runCheck);
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  await program.parseAsync(process.argv);
}
