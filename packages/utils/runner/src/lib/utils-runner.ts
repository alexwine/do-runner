import { DigitalOceanChecker } from '@do-runner/data-access-digital-ocean';
import { ConnectionConfig, MySQLChecker } from '@do-runner/data-access-mysql';

export const runner = async (config: {
  connection?: ConnectionConfig;
  do_token?: string;
  droplet_name: string;
}) => {
  const { droplet_name, do_token } = config;
  const { MYSQL_HOST, MYSQL_PORT, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE } =
    process.env;

  const connection: ConnectionConfig = config.connection || {
    host: MYSQL_HOST,
    port: parseInt(MYSQL_PORT || '', 10),
    user: MYSQL_USER,
    password: MYSQL_PASSWORD,
    database: MYSQL_DATABASE,
  };
  const mysql = MySQLChecker.getInstance(connection);
  const digital = DigitalOceanChecker.getInstance();
  try {
    const mysqlRes = await mysql.ping();
    console.log(mysqlRes);
  } catch (error) {
    console.error(error);
    const droplet = await digital.getDropletByName(droplet_name);

    if (droplet) {
      const restart = await digital.restartDroplet(droplet.id);
      console.log(restart);
    }
  }

  mysql.client.destroy();
};
