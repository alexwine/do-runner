# utils-runner

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build utils-runner` to build the library.

## Running unit tests

Run `nx test utils-runner` to execute the unit tests via [Jest](https://jestjs.io).
