import { getObjectKeys } from './utils-general';

describe('utilsGeneral', () => {
  it('should work', () => {
    const obj = {
      sample: 'something',
    };
    const res = getObjectKeys(obj);
    expect(res.length).toBe(1);
  });
});
