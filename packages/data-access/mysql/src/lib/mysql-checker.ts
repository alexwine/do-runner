import * as mysql from 'mysql';

export type ConnectionConfig = string | mysql.ConnectionConfig;

export class MySQLChecker {
  client: mysql.Connection;
  private static instance: MySQLChecker;

  private constructor(connection: ConnectionConfig) {
    this.client = mysql.createConnection(connection);
  }

  public static getInstance(connection: ConnectionConfig) {
    if (!MySQLChecker.instance) {
      MySQLChecker.instance = new MySQLChecker(connection);
    }
    return MySQLChecker.instance;
  }

  async ping() {
    // const query = this.client.query('SELECT 1 + 1 AS solution')
    const result = await new Promise<{
      message: string;
    }>((resolve, reject) => {
      this.client.ping((err) => {
        if (err) {
          reject(err);
        }
        resolve({
          message: 'success',
        });
      });
    });
    return result;
  }
}
