import { ConnectionConfig, MySQLChecker } from './mysql-checker';

describe('MySQLChecker -> Success', () => {
  let checker: MySQLChecker;

  beforeAll(() => {
    const {
      MYSQL_HOST,
      MYSQL_PORT,
      MYSQL_USER,
      MYSQL_PASSWORD,
      MYSQL_DATABASE,
    } = process.env;

    const connection: ConnectionConfig = {
      host: MYSQL_HOST,
      port: parseInt(MYSQL_PORT || '', 10),
      user: MYSQL_USER,
      password: MYSQL_PASSWORD,
      database: MYSQL_DATABASE,
    };
    checker = MySQLChecker.getInstance(connection);
  });

  test('should ping database', async () => {
    const res = await checker.ping();
    console.log(res);

    expect(res.message).toBe('success');
  });

  afterAll(() => {
    checker.client.destroy();
  });
});

describe('MySQLChecker -> Fail', () => {
  let checker: MySQLChecker;

  beforeAll(() => {
    const { MYSQL_HOST, MYSQL_PORT, MYSQL_USER, MYSQL_DATABASE } = process.env;

    const connection: ConnectionConfig = {
      host: MYSQL_HOST,
      port: parseInt(MYSQL_PORT || '', 10),
      user: MYSQL_USER,
      password: 'nothing',
      database: MYSQL_DATABASE,
    };
    checker = MySQLChecker.getInstance(connection);
  });

  test('should ping database', async () => {
    try {
      const res = await checker.ping();
      console.log(res);
    } catch (error) {
      console.log(error);

      expect(error).toBeTruthy();
    }
  });

  afterAll(() => {
    checker.client.destroy();
  });
});
