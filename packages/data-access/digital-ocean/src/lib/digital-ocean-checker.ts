import { DigitalOcean } from 'digitalocean-js';

export function digitalOceanChecker(): string {
  return 'digital-ocean-checker';
}

export class DigitalOceanChecker {
  client: DigitalOcean;
  private static instance: DigitalOceanChecker;

  private constructor() {
    const token = process.env['DO_TOKEN'] || '';

    this.client = new DigitalOcean(token);
  }

  public static getInstance() {
    if (!DigitalOceanChecker.instance) {
      DigitalOceanChecker.instance = new DigitalOceanChecker();
    }
    return DigitalOceanChecker.instance;
  }

  getDroplets = async () => {
    const res = await this.client.droplets.getAllDroplets();
    console.log(res);
    return res;
  };

  getDropletByName = async (droplet_name: string) => {
    const droplets = await this.getDroplets();
    return droplets.find((drop) => (drop.name = droplet_name));
  };

  getDropletActions = async (id: number) => {
    const res = await this.client.droplets.getDropletActions(id);
    return res;
  };
  restartDroplet = async (id: number) => {
    const action = await this.client.dropletActions.rebootDroplet(id);
    return action;
  };
}
