import {
  DigitalOceanChecker,
  digitalOceanChecker,
} from './digital-ocean-checker';

describe('digitalOceanChecker', () => {
  it('should work', () => {
    expect(digitalOceanChecker()).toEqual('digital-ocean-checker');
  });
});

const checker = DigitalOceanChecker.getInstance();

describe('DigitalOceanChecker', () => {
  test('should get droplets', async () => {
    const res = await checker.getDroplets();

    console.log(res);
  });
  test('should get actions', async () => {
    const res = await checker.getDropletActions(224994780);

    console.log(res);
  });
  test.skip('should restart droplet', async () => {
    const res = await checker.restartDroplet(177690517);

    console.log(res);
  });
  test('should return droplet by name', async () => {
    const res = await checker.getDropletByName('mysql-proxy-v2');
    console.log(res);

    expect(res?.id).toBe(177690517);
  });
});
