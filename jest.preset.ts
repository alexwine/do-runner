const nxPreset = require('@nrwl/jest/preset');
require('dotenv').config();

module.exports = {
  ...nxPreset,
  coverageReporters: ['cobertura'],
  // reporters: [
  //   'jest-junit',
  //   // [
  //   //   'jest-junit',
  //   //   {
  //   //     // outputFile: '../../coverage/<rootDir>/junit.xml',
  //   //     outputFile: path.join('..', __dirname, 'junit.xml'),
  //   //   },
  //   // ],
  // ],
  // collectCoverage: true,
};
